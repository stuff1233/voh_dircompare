﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace DirCompare
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ChangeDir(TextBox dirTextBox)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                fbd.Description = "Please select a directory to compare.";
                fbd.SelectedPath = dirTextBox.Text;

                if (fbd.ShowDialog() == DialogResult.OK)
                    dirTextBox.Text = fbd.SelectedPath;
            }
        }

        private void BrowseDir1Button_Click(object sender, EventArgs e)
        {
            ChangeDir(Dir1TextBox);
        }

        private void BrowseDir2Button_Click(object sender, EventArgs e)
        {
            ChangeDir(Dir2TextBox);
        }

        private void CompareButton_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(Dir1TextBox.Text) || !Directory.Exists(Dir2TextBox.Text))
            {
                MessageBox.Show("Unable to compare: One or more directories doesn't exist");
                return;
            }

            string[] dir1Files = Directory.GetFiles(Dir1TextBox.Text, "*.*", SearchOption.AllDirectories);
            string[] dir2Files = Directory.GetFiles(Dir2TextBox.Text, "*.*", SearchOption.AllDirectories);
            long[] dir1FileSizes = new long[dir1Files.Length];
            long[] dir2FileSizes = new long[dir2Files.Length];

            if (LimitCheckBox.Checked && (dir1Files.Length > 500 || dir2Files.Length > 500))
            {
                MessageBox.Show("Too many files being compared.");
                return;
            }

            for (int i = 0; i < dir1FileSizes.Length; i++)
                dir1FileSizes[i] = new FileInfo(dir1Files[i]).Length;

            for (int i = 0; i < dir2FileSizes.Length; i++)
                dir2FileSizes[i] = new FileInfo(dir2Files[i]).Length;

            for (int i = 0; i < dir1Files.Length; i++)
                dir1Files[i] = Path.GetFileName(dir1Files[i]);

            for (int i = 0; i < dir2Files.Length; i++)
                dir2Files[i] = Path.GetFileName(dir2Files[i]);

            OutputListBox.Items.Clear();
            OutputListBox.Items.Add("Dir 1 files that Dir 2 doesn't have:");

            for (int i = 0; i < dir1Files.Length; i++)
            {
                if (!dir2Files.Contains(dir1Files[i]))
                    OutputListBox.Items.Add(dir1Files[i]);
                else if (dir1FileSizes[i] != dir2FileSizes[IndexOf(dir2Files, dir1Files[i])])
                    OutputListBox.Items.Add("File Diff: " + dir1Files[i]);
            }

            OutputListBox.Items.Add("Dir 2 files that Dir 1 doesn't have:");

            for (int i = 0; i < dir2Files.Length; i++)
            {
                if (!dir1Files.Contains(dir2Files[i]))
                    OutputListBox.Items.Add(dir2Files[i]);
                else if (dir2FileSizes[i] != dir1FileSizes[IndexOf(dir1Files, dir2Files[i])])
                    OutputListBox.Items.Add("File Diff: " + dir2Files[i]);
            }

            OutputListBox.Items.Add("Files that have different sizes:");
        }

        private int IndexOf(string[] arr, string target)
        {
            for (int i = 0; i < arr.Length; i++)
                if (arr[i].Equals(target))
                    return i;
            return -1;
        }
    }
}
