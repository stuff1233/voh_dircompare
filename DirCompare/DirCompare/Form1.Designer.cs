﻿namespace DirCompare
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OutputListBox = new System.Windows.Forms.ListBox();
            this.Dir1TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BrowseDir1Button = new System.Windows.Forms.Button();
            this.BrowseDir2Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.Dir2TextBox = new System.Windows.Forms.TextBox();
            this.CompareButton = new System.Windows.Forms.Button();
            this.OutputButton = new System.Windows.Forms.Button();
            this.LimitCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // OutputListBox
            // 
            this.OutputListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputListBox.FormattingEnabled = true;
            this.OutputListBox.ItemHeight = 16;
            this.OutputListBox.Location = new System.Drawing.Point(4, 80);
            this.OutputListBox.Name = "OutputListBox";
            this.OutputListBox.Size = new System.Drawing.Size(491, 180);
            this.OutputListBox.TabIndex = 0;
            // 
            // Dir1TextBox
            // 
            this.Dir1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dir1TextBox.Location = new System.Drawing.Point(12, 28);
            this.Dir1TextBox.Name = "Dir1TextBox";
            this.Dir1TextBox.ReadOnly = true;
            this.Dir1TextBox.Size = new System.Drawing.Size(277, 22);
            this.Dir1TextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Directory 1";
            // 
            // BrowseDir1Button
            // 
            this.BrowseDir1Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BrowseDir1Button.Location = new System.Drawing.Point(12, 56);
            this.BrowseDir1Button.Name = "BrowseDir1Button";
            this.BrowseDir1Button.Size = new System.Drawing.Size(75, 23);
            this.BrowseDir1Button.TabIndex = 3;
            this.BrowseDir1Button.Text = "Browse";
            this.BrowseDir1Button.UseVisualStyleBackColor = true;
            this.BrowseDir1Button.Click += new System.EventHandler(this.BrowseDir1Button_Click);
            // 
            // BrowseDir2Button
            // 
            this.BrowseDir2Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BrowseDir2Button.Location = new System.Drawing.Point(497, 56);
            this.BrowseDir2Button.Name = "BrowseDir2Button";
            this.BrowseDir2Button.Size = new System.Drawing.Size(75, 23);
            this.BrowseDir2Button.TabIndex = 6;
            this.BrowseDir2Button.Text = "Browse";
            this.BrowseDir2Button.UseVisualStyleBackColor = true;
            this.BrowseDir2Button.Click += new System.EventHandler(this.BrowseDir2Button_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(500, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Directory 2";
            // 
            // Dir2TextBox
            // 
            this.Dir2TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dir2TextBox.Location = new System.Drawing.Point(295, 28);
            this.Dir2TextBox.Name = "Dir2TextBox";
            this.Dir2TextBox.ReadOnly = true;
            this.Dir2TextBox.Size = new System.Drawing.Size(277, 22);
            this.Dir2TextBox.TabIndex = 4;
            // 
            // CompareButton
            // 
            this.CompareButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CompareButton.Location = new System.Drawing.Point(501, 114);
            this.CompareButton.Name = "CompareButton";
            this.CompareButton.Size = new System.Drawing.Size(75, 23);
            this.CompareButton.TabIndex = 7;
            this.CompareButton.Text = "Compare";
            this.CompareButton.UseVisualStyleBackColor = true;
            this.CompareButton.Click += new System.EventHandler(this.CompareButton_Click);
            // 
            // OutputButton
            // 
            this.OutputButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputButton.Location = new System.Drawing.Point(501, 85);
            this.OutputButton.Name = "OutputButton";
            this.OutputButton.Size = new System.Drawing.Size(75, 23);
            this.OutputButton.TabIndex = 8;
            this.OutputButton.Text = "Output";
            this.OutputButton.UseVisualStyleBackColor = true;
            // 
            // LimitCheckBox
            // 
            this.LimitCheckBox.AutoSize = true;
            this.LimitCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LimitCheckBox.Location = new System.Drawing.Point(503, 143);
            this.LimitCheckBox.Name = "LimitCheckBox";
            this.LimitCheckBox.Size = new System.Drawing.Size(54, 20);
            this.LimitCheckBox.TabIndex = 9;
            this.LimitCheckBox.Text = "Limit";
            this.LimitCheckBox.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 261);
            this.Controls.Add(this.LimitCheckBox);
            this.Controls.Add(this.OutputButton);
            this.Controls.Add(this.CompareButton);
            this.Controls.Add(this.BrowseDir2Button);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Dir2TextBox);
            this.Controls.Add(this.BrowseDir1Button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Dir1TextBox);
            this.Controls.Add(this.OutputListBox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Compare Directories Tool";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox OutputListBox;
        private System.Windows.Forms.TextBox Dir1TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BrowseDir1Button;
        private System.Windows.Forms.Button BrowseDir2Button;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Dir2TextBox;
        private System.Windows.Forms.Button CompareButton;
        private System.Windows.Forms.Button OutputButton;
        private System.Windows.Forms.CheckBox LimitCheckBox;
    }
}

